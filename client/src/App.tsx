import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';


  type Machine = {
    floor: string;
    id: number;
    order_queue: number;
    supply_level: number;
    x: string;
    y: string;
  }
  type MachinesList = Machine[];
  type UserLocation = {
    floor?: number;
    x?: number;
    y?: number;
  }


function App() {

  const [userLocation, setUserLocation] = useState<any>({});
  //had an issue with logic in getDiffFromUserFloor() function and the UserLocation type definition (conflict with default empty object) so set state to any as workaround
  const [vendorLocations, setVendorLocations] = useState<any>([]);
  //issue with typescript and function for calculating floor distance
  const [floorsMap, setFloorsMap] = useState(new Map());
  const [floorsMapSortedDistance, setFloorsMapSortedDistance] = useState(new Map());

  const filterMachinesBySupply = (machinesList: MachinesList): MachinesList => {
    let machinesCopy = machinesList.slice(0);
    machinesCopy = machinesCopy.filter(machine => {
      return machine.supply_level > machine.order_queue
    })
    return machinesCopy;
  }

  //x1/y1 is user
  //x2/y2 is vendor
  const getDistance = (x1: number, y1: number, x2: number, y2: number): number => {
    //TODO not tested
    const xDist = Math.pow(x2 - x1, 2);
    const yDist = Math.pow(y2 - y1, 2);
    const dist = Math.sqrt(xDist + yDist);

    return dist;
  }

  //convert vendor floor to number; want absolute values
  const getDiffFromUserFloor = (machine: Machine): number | undefined => {
    //TODO not tested
    return Math.abs(userLocation.floor - parseInt(machine.floor, 10));
  }

  //returns distance between x/y points, using Pythagorean theorem
  //convert vendor x and y to number
  const getDistanceFromUserPlane = (machine: Machine): number => {
    //TODO not tested

    return getDistance(userLocation.x, userLocation.y, parseInt(machine.x, 10), parseInt(machine.y, 10));
  }

  const getMachinesMapByFloorDistance = (machinesList: MachinesList): void => {
    let mapByFloorDistance = new Map();

    if (vendorLocations.length) {
      vendorLocations.forEach((machine: Machine) => {
        const floorDiff = getDiffFromUserFloor(machine);
        if (mapByFloorDistance.has(floorDiff)) {
          mapByFloorDistance.set(floorDiff, mapByFloorDistance.get(floorDiff).concat([machine]));
        } else {
          mapByFloorDistance.set(floorDiff, [machine]); 
        }
      })
      setFloorsMap(mapByFloorDistance);      
    }
  }

  //for one floor in floorsMap, want to get the machinesList sorted by distance to the user (assuming same plane)
  //think this requires distToIdMap and idToMachineMap
  const getSortedMachinesList = (machinesList: MachinesList): Machine[] => {
    let machineToDistMap = new Map();
    let distToIdMap = new Map();
    let idToMachineMap = new Map();
    let sortedMachines: Machine[] = [];

    machinesList.forEach(machine => {
      const k = machine.id;
      const dist = getDistanceFromUserPlane(machine);
      machineToDistMap.set(k, dist);
      distToIdMap.set(dist, k);
      idToMachineMap.set(k, machine);
    })

    let machineDistances: any = [];

    machineToDistMap.forEach((value, key, map) => {
      machineDistances.push(value);
    });
    machineDistances.sort();

    machineDistances.forEach((dist: number) => {
      const id = distToIdMap.get(dist);
      sortedMachines.push(idToMachineMap.get(id));
    })

    return sortedMachines;
  }

  const getSortedMachinesListByFloor = (): void => {
    let floorToSortedMachinesMap = new Map();
    floorsMap.forEach((value, key, map) => {
      const sortedMachinesForFloor = getSortedMachinesList(value);
      floorToSortedMachinesMap.set(key, sortedMachinesForFloor);
    });
    setFloorsMapSortedDistance(floorToSortedMachinesMap);
  }

  useEffect(() => {
    fetch(`http://localhost:8000/location`)
      .then(resp => {
        try {
          if (resp.ok) {
            return resp.json();
          } 
        } catch (e) {
          console.error(e);
          console.error(`${resp.status}: ${resp.statusText}`)
        }
      })
      .then(json => {
        setUserLocation(json);
      })

    fetch(`http://localhost:8000/machines`)
      .then(resp => {
        try {
          if (resp.ok) {
            return resp.json();
          } 
        } catch (e) {
          console.error(e);
          console.error(`${resp.status}: ${resp.statusText}`)
        }
      })
      .then(json => {
        //remove machines with supply too low
        let machinesCopy = filterMachinesBySupply(json);
        setVendorLocations(machinesCopy);
      })
  }, []);

  //get sorted list of floor distances from floorsMapSortedDistance (keys)
  //create an array of machines starting from lowest floor distance to highest floor distance
  const getLatestSortedMachinesListByFloorDistance = (): Machine[] => {
    let floorDistances: number[] = [];
    let sortedMachinesFlat: Machine[] = [];

    if (floorsMapSortedDistance.size) {
      floorsMapSortedDistance.forEach((value, key, map) => {
        floorDistances.push(key);
      });

      floorDistances.sort();
      floorDistances.forEach(dist => {
        sortedMachinesFlat = sortedMachinesFlat.concat(floorsMapSortedDistance.get(dist));
      });      
    }

    return sortedMachinesFlat;
  }

  useEffect(() => {
    //apply sorting logic to the vendor locations to return

    if (Object.keys(userLocation) && vendorLocations) {
      getMachinesMapByFloorDistance(vendorLocations);
    }
  }, [vendorLocations, userLocation]);

  useEffect(() => {
    getSortedMachinesListByFloor();
  }, [floorsMap])

  const latestSortedMachinesList = getLatestSortedMachinesListByFloorDistance();
  //if populated, then display the first machine to the UI


  if (!latestSortedMachinesList.length) {
    return (
      <div>
        <p>Searching for a coffee machine...</p>
      </div>
    )
  } else {
    return (
      <div className="App">
        <h1>Coffee Machine Locator</h1>

        {latestSortedMachinesList.length > 0} {
          <div>
            <p>Good morning!</p>
            <p>The closest machine is at floor {latestSortedMachinesList[0].floor} and position x: {latestSortedMachinesList[0].x} and y: {latestSortedMachinesList[0].y} </p>
          </div>
        }
      </div>
    );
  }
}

export default App;
